import itertools

from pesummary.gw.plots.publication import twod_contour_plots
from pesummary.gw.plots.latex_labels import GWlatex_labels

from .base_parsers import parser_parent_2d, parser_parent_contour
from .colors import get_color_cycle

def populate_subparser(subparsers, subcommand):
    parents = [parser_parent_2d, parser_parent_contour]
    subparser = subparsers.add_parser(subcommand, parents=parents)

    subparser.set_defaults(plot_func=main)

    return subparser


def main(events, cli_args):
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()

    param_name_x, param_name_y = cli_args.parameters

    ax.set_xlabel(GWlatex_labels[param_name_x])
    ax.set_ylabel(GWlatex_labels[param_name_y])

    n_events = len(events)

    colors = list(
        itertools.islice(get_color_cycle(cli_args.color_cycle), n_events)
    )
    samples = [
        [event[param_name_x], event[param_name_y]]
        for event in events
    ]
    fake_labels = [""]*n_events

    # Make 2-D contour plots with PESummary
    fig, ax = twod_contour_plots(
        cli_args.parameters, samples, fake_labels, GWlatex_labels,
        colors=colors, return_ax=True,
    )

    # Remove overly busy legend
    ax.get_legend().remove()

    return fig
