import argparse
import importlib


subcommands = (
    "scatter_2d",
    "scatter_3d",
    "contour_2d",
    "point_3d",
)

def make_parser():
    parser = argparse.ArgumentParser(add_help=False)

    subparsers = parser.add_subparsers(dest="command")

    for subcommand in subcommands:
        module = importlib.import_module("gwtc_explorer.plotting."+subcommand)
        module.populate_subparser(subparsers, subcommand)

    return parser

parser = make_parser()


def plot_cli(events, cli_args):
    cli_args.setup_func(cli_args)
    fig = cli_args.plot_func(events, cli_args)
    cli_args.save_func(fig, cli_args)
