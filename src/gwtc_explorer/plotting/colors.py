import itertools
from matplotlib.colors import ListedColormap

def get_color_cycle(cmap_name):
    import matplotlib.pyplot as plt

    cmap = plt.get_cmap(cmap_name)

    if not isinstance(cmap, ListedColormap):
        raise TypeError("Colormap must be a ListedColormap")

    return itertools.cycle(cmap.colors)
