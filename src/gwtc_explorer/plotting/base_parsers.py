import argparse
from ..data import parser as data_parser

def setup_plot_2d(cli_args):
    if cli_args.mpl_backend is not None:
        import matplotlib as mpl
        mpl.use(cli_args.mpl_backend)
    import matplotlib.pyplot as plt

def setup_plot_3d(cli_args):
    if cli_args.mpl_backend is not None:
        import matplotlib as mpl
        mpl.use(cli_args.mpl_backend)
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt


def save_plot(fig, cli_args):
    import matplotlib.pyplot as plt

    if cli_args.save_fig is not None:
        fig.savefig(cli_args.save_fig)

    if (cli_args.save_fig is None) or cli_args.interactive:
        plt.show()


def make_parser_parent():
    parents = [data_parser]
    parser = argparse.ArgumentParser(parents=parents, add_help=False)

    group = parser.add_argument_group(
        "Output",
        "Options for outputting results.",
    )

    group.add_argument(
        "-i", "--interactive",
        action="store_true",
        help="Display plot interactively.  Enabled if no other output option "
             "is selected.",
    )

    group.add_argument(
        "-s", "--save-fig",
        metavar="FILE",
        help="Store figure to file.",
    )

    group.add_argument(
        "--mpl-backend",
        help="Set backend for matplotlib.",
    )

    parser.set_defaults(save_func=save_plot)

    return parser

parser_parent = make_parser_parent()


def make_parser_parent_2d():
    parser = argparse.ArgumentParser(parents=[parser_parent], add_help=False)

    parser.add_argument(
        "-p", "--parameters",
        nargs=2, metavar=("PARAM_X", "PARAM_Y"),
        default=("mass_1_source", "mass_2_source"),
        help="Parameters to plot.  "
             "Default is mass_1_source mass_2_source",
    )

    parser.set_defaults(setup_func=setup_plot_2d)

    return parser

parser_parent_2d = make_parser_parent_2d()


def make_parser_parent_3d():
    parser = argparse.ArgumentParser(parents=[parser_parent], add_help=False)

    parser.add_argument(
        "-p", "--parameters",
        nargs=3, metavar=("PARAM_X", "PARAM_Y", "PARAM_Z"),
        default=("mass_1_source", "mass_2_source", "chi_eff"),
        help="Parameters to plot.  "
             "Default is mass_1_source mass_2_source chi_eff",
    )

    parser.set_defaults(setup_func=setup_plot_3d)

    return parser

parser_parent_3d = make_parser_parent_3d()


def make_parser_parent_color_cycle():
    import matplotlib.colors as c

    cmaps = [
        "Pastel1", "Pastel2", "Paired", "Accent",
        "Dark2", "Set1", "Set2", "Set3",
        "tab10", "tab20", "tab20b", "tab20c",
    ]

    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument(
        "--color-cycle",
        metavar="NAME",
        default="tab20", choices=cmaps,
        help="Color cycle for events.  "
             "Default is 'tab20'.  "
             "Must be one of: {}".format(cmaps),
    )

    return parser

parser_parent_color_cycle = make_parser_parent_color_cycle()


def make_parser_parent_scatter():
    parents = [parser_parent_color_cycle]
    parser = argparse.ArgumentParser(parents=parents, add_help=False)

    parser.add_argument(
        "--prune-samples", metavar="MAX_SAMPLES",
        type=int,
        help="Maximum number of samples to include in a scatter plot.",
    )

    parser.add_argument(
        "--marker",
        default=".",
        help="Marker to use in scatter plot.",
    )

    parser.add_argument(
        "--marker-size",
        type=float,
        default=1,
        help="Marker size to use in scatter plot.",
    )

    return parser

parser_parent_scatter = make_parser_parent_scatter()


def make_parser_parent_contour():
    parents = [parser_parent_color_cycle]
    parser = argparse.ArgumentParser(parents=parents, add_help=False)

    parser.add_argument(
        "--prune-samples", metavar="MAX_SAMPLES",
        type=int,
        help="Maximum number of samples to include in a scatter plot.",
    )

    parser.add_argument(
        "--levels", metavar="LEVELS",
        type=int, default=1,
        help="Number of contour levels to include.  Default is 1.",
    )

    return parser

parser_parent_contour = make_parser_parent_contour()
