from pesummary.gw.plots.latex_labels import GWlatex_labels

from .base_parsers import parser_parent_2d, parser_parent_scatter
from .colors import get_color_cycle

def populate_subparser(subparsers, subcommand):
    parents = [parser_parent_2d, parser_parent_scatter]
    subparser = subparsers.add_parser(subcommand, parents=parents)

    subparser.set_defaults(plot_func=main)

    return subparser


def main(events, cli_args):
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()

    param_name_x, param_name_y = cli_args.parameters

    ax.set_xlabel(GWlatex_labels[param_name_x])
    ax.set_ylabel(GWlatex_labels[param_name_y])

    colors = get_color_cycle(cli_args.color_cycle)

    for event, color in zip(events, colors):
        samples_x = event[param_name_x]
        samples_y = event[param_name_y]

        if cli_args.prune_samples is not None:
            # TODO: randomize before pruning
            samples_x = samples_x[:cli_args.prune_samples]
            samples_y = samples_y[:cli_args.prune_samples]

        ax.scatter(
            samples_x, samples_y,
            color=color,
            marker=cli_args.marker, s=cli_args.marker_size,
        )

    return fig
