import warnings
import os
import numpy

from pesummary.io import read
from pesummary.gw.fetch import fetch_open_data


def load(event_name, outdir=None):
    # Construct expected name of downloaded file.  Cannot do this if output is
    # in a temporary directory.
    target_basename = "{name}_GWTC-1.hdf5".format(name=event_name)
    target_fname = None
    if outdir is not None:
        target_fname = os.path.join(outdir, target_basename)

    # Fetch file if it is not already downloaded.
    if (outdir is not None) and (not os.path.exists(target_fname)):
        # Fetch file.
        target_fname = fetch_open_data(
            event_name,
            delete_on_exit=False, outdir=outdir, read_file=False,
        )

    # Open file.
    pesummary_read = read(target_fname)

    # Generate all derived quanitites.
    pesummary_read.generate_all_posterior_samples()

    # Extract posterior samples as dict.
    samples_arr = numpy.asarray(pesummary_read.samples)
    result = dict(zip(pesummary_read.parameters, samples_arr.T))

    # HACK: Add on chi_eff as result as PESummary misses it for some reason.
    if "chi_eff" not in result:
        warnings.warn(
            "PESummary failed to provide 'chi_eff' for {}, recomputing now."
            .format(event_name)
        )
        m1 = result["mass_1"]
        a1z = result["a_1"] * result["cos_tilt_1"]
        m2 = result["mass_2"]
        a2z = result["a_2"] * result["cos_tilt_2"]
        result["chi_eff"] = (m1*a1z + m2*a2z) / (m1+m2)

    # Return result
    return result
