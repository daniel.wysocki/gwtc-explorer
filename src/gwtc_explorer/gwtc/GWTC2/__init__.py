import warnings
import os
import numpy
import glob

from pesummary.io import read
from pesummary.gw.fetch import fetch_open_data


def load(event_name, outdir=None):
    # Construct expected name of downloaded file.  Cannot do this if output is
    # in a temporary directory.
    target_basename = ".".join([event_name, "h5"])
    target_fname = None
    if outdir is not None:
        target_fname = os.path.join(outdir, event_name, target_basename)

    # Fetch file if it is not already downloaded.
    if (outdir is not None) and (not os.path.exists(target_fname)):
        # Fetch archive containing desired file as well as extras.
        unpacked_dir = fetch_open_data(
            event_name, catalog="GWTC-2",
            delete_on_exit=False, outdir=outdir, unpack=True, read_file=False,
        )

        # Remove all files except for the actual posterior samples.
        for fname in glob.glob(os.path.join(unpacked_dir, "*")):
            if os.path.basename(fname) != target_basename:
                os.remove(fname)

        # Construct path to file.
        target_fname = os.path.join(unpacked_dir, target_basename)

    # Open file.
    pesummary_read = read(target_fname)

    # Get index of 'PublicationSamples' label.
    # TODO: allow for specifying analysis
    analysis_label = pesummary_read.labels.index("PublicationSamples")

    # Extract posterior samples as dict.
    samples_arr = numpy.asarray(pesummary_read.samples[analysis_label])
    result = dict(zip(pesummary_read.parameters[analysis_label], samples_arr.T))

    # HACK: Add on chi_eff to result as it is sometimes absent.
    if "chi_eff" not in result:
        warnings.warn(
            "PESummary failed to provide 'chi_eff' for {}, recomputing now."
            .format(event_name)
        )
        m1 = result["mass_1"]
        a1z = result["a_1"] * result["cos_tilt_1"]
        m2 = result["mass_2"]
        a2z = result["a_2"] * result["cos_tilt_2"]
        result["chi_eff"] = (m1*a1z + m2*a2z) / (m1+m2)

    # Return result
    return result
