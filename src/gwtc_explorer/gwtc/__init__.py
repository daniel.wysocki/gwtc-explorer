from . import (
    GWTC1,
    GWTC2,
)

_catalogs = (
    ...,
    GWTC1,
    GWTC2,
)

def get_catalog_range(catalog_min=None, catalog_max=None):
    # Validate inputs.
    if catalog_min is not None:
        if catalog_min < 1:
            raise ValueError("Cannot have a catalog earlier than 1.")
    if catalog_max is not None:
        if catalog_max > len(_catalogs):
            raise ValueError(
                "Cannot have a catalog later than {}.".format(len(_catalogs))
            )
    if (catalog_min is not None) and (catalog_max is not None):
        if catalog_min > catalog_max:
            raise ValueError(
                "Minimum catalog cannot be higher than maximum catalog.",
            )

    # Get appropriate slice.
    lower = 1 if catalog_min is None else catalog_min
    upper = None if catalog_max is None else catalog_max+1
    slc = slice(lower, upper)

    # Return range of catalog modules.
    return _catalogs[slc]
