import typing
import argparse
import traceback
import os
import json
import importlib.resources

from .gwtc import get_catalog_range

def make_parser():
    parser = argparse.ArgumentParser(add_help=False)

    group = parser.add_argument_group(
        "Events",
        "Options for selecting events.",
    )

    group.add_argument(
        "--event-dir",
        help="Directory to store events in.  Default is to use a temporary "
             "location that will be deleted as soon as the events are loaded "
             "into memory.",
    )

    group.add_argument(
        "--catalog-min",
        type=int,
        help="Oldest catalog to plot.",
    )
    group.add_argument(
        "--catalog-max",
        type=int,
        help="Latest catalog to plot.",
    )

    group.add_argument(
        "--far",
        type=float,
        help="False-alarm-rate threshold in units of yr^-1.",
    )

    return parser

parser = make_parser()


def fetch(
        outdir: typing.Optional[str]=None,
        catalog_min: typing.Optional[str]=None,
        catalog_max: typing.Optional[str]=None,
        far: typing.Optional[float]=None,
        snr: typing.Optional[float]=None,
        dq: typing.Optional[str]=None,
        searches: typing.Optional[typing.Tuple[str,...]]=None,
    ):
    catalogs = get_catalog_range(
        catalog_min=catalog_min, catalog_max=catalog_max,
    )

    events = []
    for catalog in catalogs:
        files = importlib.resources.contents(catalog)

        for fname in files:
            # Skip non-JSON files.
            if not fname.lower().endswith(".json"):
                continue

            # Get event name.
            event_name = os.path.basename(fname)[:-len(".json")]

            # Load info from event.
            with importlib.resources.open_text(catalog, fname) as f:
                event_info = json.load(f)

            # Skip if above FAR threshold.
            if not far_pass(event_info, far, searches):
                continue

            # Skip if below SNR threshold.
            if not snr_pass(event_info, snr, searches):
                continue

            # Skip if fails DQ criteria.
            # TODO

            # All checks passed, load data with PESummary
            try:
                events.append(catalog.load(event_name, outdir=outdir))
            except Exception as e:
                traceback.print_tb(e.__traceback__)
                print(e)

    return events


def fetch_cli(cli_args):
    return fetch(
        outdir=cli_args.event_dir,
        catalog_min=cli_args.catalog_min,
        catalog_max=cli_args.catalog_max,
        far=cli_args.far,
    )


def far_pass(event_info, far_threshold, searches):
    if far_threshold is None:
        return True

    fars = event_info["FAR"]

    if searches is None:
        searches = fars.keys()

    for search in searches:
        if search in fars:
            if fars[search] <= far_threshold:
                return True

    return False


def snr_pass(event_info, snr_threshold, searches):
    if snr_threshold is None:
        return True

    snrs = event_info["SNR"]

    for search in searches:
        if search in snrs:
            if snrs[search] >= snr_threshold:
                return True

    return False
