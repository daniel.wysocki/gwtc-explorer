import os
import argparse
from .data import fetch_cli
from .plotting import parser as plotting_parser, plot_cli

def make_parser():
    parents = [plotting_parser]
    parser = argparse.ArgumentParser(parents=parents)

    return parser

parser = make_parser()


def main():
    cli_args = parser.parse_args()

    # Ensure output directory exists if provided.
    if cli_args.event_dir is not None:
        try:
            os.mkdir(cli_args.event_dir)
        except FileExistsError as e:
            pass
        except FileNotFoundError as e:
            raise FileNotFoundError(
                "Failed to create output directory {}"
                .format(cli_args.event_dir)
            )

    events = fetch_cli(cli_args)
    plot_cli(events, cli_args)
