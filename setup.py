"""GWTC-explorer

Tools for exploring the gravitational wave transient catalogs.
"""

from datetime import date


#-------------------------------------------------------------------------------
#   GENERAL
#-------------------------------------------------------------------------------
__name__        = "gwtc_explorer"
__version__     = "0.0.2"
__date__        = date(2022, 1, 12)
__keywords__    = [
    "astronomy",
    "information analysis",
    "physics",
]
__status__      = "Alpha"


#-------------------------------------------------------------------------------
#   URLS
#-------------------------------------------------------------------------------
__url__         = "https://git.ligo.org/daniel.wysocki/gwtc-explorer"
__bugtrack_url__= "https://git.ligo.org/daniel.wysocki/gwtc-explorer/issues"


#-------------------------------------------------------------------------------
#   PEOPLE
#-------------------------------------------------------------------------------
__author__      = "Daniel Wysocki"
__author_email__= "daniel.wysocki@ligo.org"

__maintainer__      = "Daniel Wysocki"
__maintainer_email__= "daniel.wysocki@ligo.org"

__credits__     = ("Daniel Wysocki",)


#-------------------------------------------------------------------------------
#   LEGAL
#-------------------------------------------------------------------------------
__copyright__   = 'Copyright (c) 2021-2022 {author} <{email}>'.format(
    author=__author__,
    email=__author_email__
)

__license__     = 'MIT License'
__license_full__= '''
MIT License

{copyright}

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''.format(copyright=__copyright__).strip()


#-------------------------------------------------------------------------------
#   PACKAGE
#-------------------------------------------------------------------------------
DOCLINES = __doc__.split("\n")

CLASSIFIERS = """
Development Status :: 3 - Alpha
Programming Language :: Python :: 3
Operating System :: OS Independent
Intended Audience :: Science/Research
Topic :: Scientific/Engineering :: Astronomy
Topic :: Scientific/Engineering :: Physics
Topic :: Scientific/Engineering :: Information Analysis
""".strip()

REQUIREMENTS = {
    "install" : [
        "pesummary>=0.13.3,<0.14.0",
        "numpy>1.13.0,<2.0.0",
        "matplotlib>=3.0.0,<4.0.0",
        "h5py>=2.7.0,<4.0.0",
        "frozendict>=1.0.0,<2.0.0",
    ],
    "setup" : [
        "pytest-runner",
    ],
    "tests" : [
        "pytest",
    ]
}

ENTRYPOINTS = {
    "console_scripts" : [
        "gwtc_explorer = gwtc_explorer.cli:main",
    ]
}

from setuptools import find_packages, setup

metadata = dict(
    name        =__name__,
    version     =__version__,
    description =DOCLINES[0],
    long_description='\n'.join(DOCLINES[2:]),
    keywords    =__keywords__,

    author      =__author__,
    author_email=__author_email__,

    maintainer  =__maintainer__,
    maintainer_email=__maintainer_email__,

    url         =__url__,
#    download_url=__download_url__,

    license     =__license__,

    classifiers=[f for f in CLASSIFIERS.split('\n') if f],

    package_dir ={"": "src"},
    packages=find_packages("src"),
    package_data={
        "gwtc_explorer.gwtc" : ["**/*.json"],
    },

    install_requires=REQUIREMENTS["install"],
    setup_requires=REQUIREMENTS["setup"],
    tests_require=REQUIREMENTS["tests"],

    entry_points=ENTRYPOINTS,
)

setup(**metadata)
